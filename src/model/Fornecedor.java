package model;

public class Fornecedor implements Runnable{
	private Conta umaConta;
	
	public Fornecedor() {
		this.umaConta = null;
	}
	public  Fornecedor(Conta umaConta) {
		this.umaConta = umaConta;
	}
	@Override
	public void run() {
		synchronized(umaConta) {
			umaConta.notify();/* ACORDA THREAD QUE ESTA COM WAIT POREM NAO LIBERA LOCK E FORNECEDOR TERMINA BLOCO SYNCHRONIZED*/
			for(int i = 0; i<3; i++) {
				umaConta.depositar(100);
			}	
		}
		

	}
	
	public Conta getUmaConta() {
		return umaConta;
	}

	public void setUmaConta(Conta umaConta) {
		this.umaConta = umaConta;
	}

}

