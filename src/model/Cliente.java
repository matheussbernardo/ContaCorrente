package model;

public class Cliente implements Runnable {
	private Conta umaConta;
	public Cliente() {
		this.setUmaConta(null);
	}
	public Cliente(Conta umaConta) {
		this.setUmaConta(umaConta);
	}
	@Override
	public void run() {
		synchronized(umaConta) {
			umaConta.notify();/* ACORDA THREAD QUE ESTA COM WAIT POREM NAO LIBERA LOCK E CLIENTE TERMINA BLOCO SYNCHRONIZED*/
			for(int i=0; i<3; i++) {
				try {
					umaConta.sacar(100);
				} catch (SaqueExcpetion e) {
					e.printStackTrace();
				}
			}
		}
	}
	public Conta getUmaConta() {
		return umaConta;
	}
	public void setUmaConta(Conta umaConta) {
		this.umaConta = umaConta;
	}

}
