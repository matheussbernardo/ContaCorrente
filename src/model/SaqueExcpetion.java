package model;

public class SaqueExcpetion extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public SaqueExcpetion() { super(); }
	public SaqueExcpetion(String message) { super(message); }
	public SaqueExcpetion(String message, Throwable cause) { super(message, cause); }
	public SaqueExcpetion(Throwable cause) { super(cause); }	
}
