package model;

public class Conta {
	private double saldo;
	
	public Conta() {
		super();
		this.saldo = 0.0;
	}

	public Conta(double saldo) {
		super();
		this.saldo = saldo;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	
	public void depositar(double valor) {
		double temp;
		temp = getSaldo();
		saldo = temp+valor;
	}
	public void sacar(double valor) throws SaqueExcpetion  {
		double temp;
		temp = getSaldo();
		if(temp<valor) {
			throw new SaqueExcpetion("VALOR INDISPONIVEL!!!");
		}
		saldo = temp-valor;

	}
}
