package view;

import model.Cliente;
import model.Conta;
import model.Fornecedor;

public class Banco {

	private static Cliente umCliente;
	private static Fornecedor umFornecedor;

	public static void main(String[] args) {
		Conta umaConta = new Conta();

		umFornecedor = new Fornecedor(umaConta);
		Thread threadFornecedor = new Thread(umFornecedor);		

		umCliente = new Cliente(umaConta);
		Thread threadCliente = new Thread(umCliente);
		
		/* INICIA THREAD FORNECEDOR*/
		threadFornecedor.start();
		/*MAIN ADQUIRE LOCK DE umaConta E FAZ FORNECEDOR FICAR ESPERANDO LOCK*/
		synchronized(umaConta) {			
			try {
				umaConta.wait();/*THREAD FORNECEDOR ESTAVA ESPERANDO MAIN LIBERAR LOCK E 
									AGORA MAIN LIBERA LOCK E ESPERA THREAD FORNECEDOR NOTIFICAR QUE ACABOU*/
			} catch (InterruptedException e) {
				e.printStackTrace();
			}		
		}
		/*NESSE PONTO THREAD FORNECEDOR TERMINOU E MAIN CONTINUA POIS FORNECEDOR ACORDOU A MAIN COM NOTIFY E C/ ISSO MAIN ADQUIRE LOCK AUTOMATICAMENTE*/
		/*INICIA THREAD CLIENTE COM A CERTEZA QUE THREAD FORNECEDOR ACABOU*/
		threadCliente.start();
		/*MAIN ADQUIRE LOCK DE umaConta E FAZ CLIENTE ESPERAR LOCK*/
		synchronized (umaConta) {
			try {
				umaConta.wait();/*MAIN LIBERA LOCK E CLIENTE ADQUIRE LOCK POIS ESTAVA ESPERANDO, AGORA THREAD MAIN ESPERA CLIENTE NOTIFICAR QUE ACABOU*/
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		/*NESSE PONTO CLIENTE TERMINOU E MAIN ADQUIRE LOCK POIS CLIENTE ACORDOU COM NOTIFY*/
		/* THREAD MAIN CONTINUA, E PRINTA SALDO COM A CERTEZA QUE OS DOIS THREADS TERMINARAM E QUE NÃO OCORRERAM AO MESMO TEMPO*/	
		System.out.println("SALDO FINAL: "+umaConta.getSaldo());
		
	}
}
